<?xml version="1.0"?>
<package xmlns="http://schemas.microsoft.com/packaging/2010/07/nuspec.xsd">
    <metadata>
        <id>Trestle.Json.Rpc</id>
        <title>Trestle.Json.Rpc</title>
        <version>0.0.10</version>
        <authors>Lou Parslow</authors>
        <owners>Lou Parslow</owners>
        <licenseUrl>https://gitlab.com/trestle/trestle/raw/master/LICENSE</licenseUrl>
        <projectUrl>https://gitlab.com/trestle/trestle</projectUrl>
        <iconUrl>https://gitlab.com/trestle/trestle/raw/master/trestle.png</iconUrl>
        <requireLicenseAcceptance>false</requireLicenseAcceptance>
        <summary>Json-Rpc.</summary>
        <description>
            .NET Library to support to provide support for JSON RPC.

            Supported platforms:
            - .NET Standard 2.0
        </description>
        <releaseNotes></releaseNotes>
        <language>en-US</language>
        <tags>JSON</tags>
        <copyright>Copyright (c) 2018 Lou Parslow</copyright>
        <dependencies>
          <group targetFramework="netstandard20">
            <dependency id="Trestle.Serialization.Json" version="0.0.19" />
            <dependency id="Trestle.Bindings" version="0.0.7" />
            <dependency id="Trestle.Extensions" version="0.0.34" />
          </group>
        </dependencies>
    </metadata>
    <files>
        <file src="LICENSE" />
        <file src="Trestle.Json.Rpc/bin/Release/netstandard2.0/Trestle.Json.Rpc.dll" target="lib\netstandard20" />
    </files>
</package>
