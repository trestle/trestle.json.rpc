﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using Trestle.Extensions;
using Trestle.Serialization.Json;

namespace Trestle.Json.Rpc
{
	[TestFixture]
	internal static class ResponderTest
	{
		[Test]
		public static void Default_Usage()
		{
			var responder = new Responder();
			var requests = TestRequests;
			foreach (var name in requests.Keys)
			{
				var request = TestRequests[name];
				var response = responder.Respond(request);
				Assert.NotNull(response, nameof(response));
			}
		}

		[Test]
		public static void Custom_Usage()
		{
			var responder = new Responder
			{
				Resolvers = new Dictionary<string, Resolver>
				{
					{"add", new Resolver(new Trestle.Bindings.Function<int,int,int>(Add)) }
				}
			};
			var requests = TestRequests;
			foreach (var name in requests.Keys)
			{
				var request = TestRequests[name];
				var response = responder.Respond(request);
				Assert.NotNull(response, nameof(response));
				if (TestResponses.ContainsKey(name))
				{
					var expected_json = TestResponses[name].ToJson();
					var actual_json = response.ToJson();
					Assert.AreEqual(expected_json, actual_json, name);
				}
			}
		}

		[Test]
		public static void Exceptions_Coverage()
		{
			var responder = new Responder
			{
				Resolvers = new Dictionary<string, Resolver>
				{
					{ "raise_exception", new Resolver(
						new Bindings.Action(ThrowException))}
				}
			};

			Assert.NotNull(responder.Respond(new Request("raise_exception")));
			var response = responder.Respond(new Request("not_there"));
			Assert.NotNull(response);
			Assert.True(response.Error.Message.Contains("not found"));
			Assert.AreEqual((int)ErrorCode.MethodNotFound,response.Error.Code);

			response = responder.Respond(new Request("raise_exception", new object[] { 0, 1 }));
			Assert.AreEqual((int)ErrorCode.InvalidParameters, response.Error.Code);
		}

		private static void ThrowException() { throw new InvalidOperationException(); }

		private static int Add(int a, int b)
		{
			return a + b;
		}

		public static IDictionary<string, Request> TestRequests
		{
			get
			{
				var stream = typeof(ResponderTest).Assembly.FindManifestResourceStream("Responder.Test.Requests.json");
				return new Trestle.Serialization.Json.JsonFormatter().Deserialize<IDictionary>(stream).ConvertValues<Request>();
			}
		}

		public static IDictionary<string, Response> TestResponses
		{
			get
			{
				var stream = typeof(ResponderTest).Assembly.FindManifestResourceStream("Responder.Test.Responses.json");
				return new Trestle.Serialization.Json.JsonFormatter().Deserialize<IDictionary>(stream).ConvertValues<Response>();
			}
		}
	}
}