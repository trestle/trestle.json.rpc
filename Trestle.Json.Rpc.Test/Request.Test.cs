﻿using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using Trestle.Extensions;

namespace Trestle.Json.Rpc.Test
{
	[TestFixture]
	internal class RequestTest
	{
		[Test]
		public void Construction()
		{
			var request = new Request("test");
			Assert.NotNull(request.Clone(),"request.Clone");

			var request2 = new Request("Test", new Dictionary<string, object> { { "id", null } });
			Assert.NotNull(request2.Clone(), "request2.Clone()");
		}

		[Test]
		public static void Coverage()
		{
			var instances = TestInstances;
			foreach (string key in instances.Keys)
			{
				instances[key].Clone();
			}
		}

		[Test]
		public static void GetParameterArray()
		{
			var request = new Request("add", new object[] { 2, 3 });
			var parameters = request.GetParameterArray();
			Assert.AreEqual(2, parameters.Length);
		}

		public static IDictionary<string, Request> TestInstances
		{
			get
			{
				var stream = typeof(RequestTest).Assembly.FindManifestResourceStream("Request.Test.json");
				return new Trestle.Serialization.Json.JsonFormatter().Deserialize<IDictionary>(stream).ConvertValues<Request>();
			}
		}
	}
}