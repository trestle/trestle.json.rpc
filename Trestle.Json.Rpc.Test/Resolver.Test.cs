using NUnit.Framework;
using Trestle.Serialization.Json;

namespace Trestle.Json.Rpc
{
	[TestFixture]
	public static class ResolverTest
	{
		[Test]
		public static void Add()
		{
			var resolver = new Resolver(
				new Trestle.Bindings.Function<int, int, int>(_Add));
			var request = ResponderTest.TestRequests["add"];
			var response = resolver.Respond(request);
			var expected_response = ResponderTest.TestResponses["add"];

			Assert.AreEqual(expected_response.ToJson(), response.ToJson());
		}

		private static int _Add(int a, int b)
		{
			return a + b;
		}
	}
}