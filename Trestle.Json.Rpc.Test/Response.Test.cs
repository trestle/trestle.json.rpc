﻿using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using Trestle.Extensions;
using Trestle.Serialization.Json;

namespace Trestle.Json.Rpc.Test
{
	[TestFixture]
	internal static class ResponseTest
	{
		[Test]
		public static void Coverage()
		{
			var instances = TestInstances;
			foreach (string key in instances.Keys)
			{
				var json = instances[key].ToJson();
				Assert.True(json.Length > 5, json);
				instances[key].Clone();
			}
		}

		[Test]
		public static void Construction()
		{
			var r = new Response(1);
			Assert.NotNull(r);
			r = new Response("true", 1);
			Assert.NotNull(r);
			r = new Response(new Error(ErrorCode.InternalError), 1);
			Assert.NotNull(r);
		}

		public static IDictionary<string, Response> TestInstances
		{
			get
			{
				var stream = typeof(ResponseTest).Assembly.FindManifestResourceStream("Response.Test.json");
				return new Trestle.Serialization.Json.JsonFormatter().Deserialize<IDictionary>(stream).ConvertValues<Response>();
			}
		}
	}
}