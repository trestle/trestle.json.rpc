﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using Trestle.Extensions;

namespace Trestle.Json.Rpc.Test
{
	[TestFixture]
	internal static class ErrorTest
	{
		public static IDictionary<string, Error> TestInstances
		{
			get
			{
				var stream = typeof(ErrorTest).Assembly.FindManifestResourceStream("Error.Test.json");
				return new Trestle.Serialization.Json.JsonFormatter().Deserialize<IDictionary>(stream).ConvertValues<Error>();
			}
		}

		[Test]
		public static void Construction()
		{
			new Error(ErrorCode.InternalError);
			var error = new Error(ErrorCode.InternalError, "internal error");
			var error2 = error.Clone();
			Assert.AreEqual((int)ErrorCode.InternalError, error2.Code);

			error = new Error(ErrorCode.MethodNotFound, "method not found", "add");
			error2 = error.Clone();
			Assert.AreEqual((int)ErrorCode.MethodNotFound, error2.Code);

			foreach (ErrorCode code in ((ErrorCode[])Enum.GetValues(typeof(ErrorCode))))
			{
				new Error(code);
			}
		}

		[Test]
		public static void Coverage()
		{
			var instances = TestInstances;
			foreach (string key in instances.Keys)
			{
				instances[key].Clone();
			}
		}
	}
}