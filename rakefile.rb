NAME='Trestle.Json.Rpc'
VERSION='0.0.11'
require 'dev'

#task :update_NuGet_Versions do
#	Nuget.update_versions("#{NAME}/#{NAME}.csproj","#{NAME}.nuspec")
#end

task :setup do#=> [:update_NuGet_Versions] do
    Setup.setupStandardClassLib(NAME,'C#') if(!Dir.exists?(NAME))
	Dir.chdir("#{NAME}.Test") do
		puts `dotnet add package coverlet.msbuild`
	end

	yml_version=IO.read('.gitlab-ci.yml').scan(/\/v:"([\d.]+)"/)[0][0]
	puts "yml_version = #{yml_version}"
	Text.replace_in_file('.gitlab-ci.yml',"v:\"#{yml_version}\"","v:\"#{VERSION}\"")
	puts `nuget restore #{NAME}.sln`
end

task :build do
	puts `dotnet build --configuration Debug`
	puts `dotnet build --configuration Release`
end

task :test do
    puts `dotnet test #{NAME}.Test/#{NAME}.Test.csproj -f netcoreapp2.0`
	puts `dotnet test #{NAME}.Test/#{NAME}.Test.csproj -f net46`
end

task :analyze => [:setup] do
	puts `dotnet sonarscanner begin /k:"#{NAME}" /d:sonar.organization="trestle" /d:sonar.host.url="https://sonarcloud.io" /d:sonar.login="b74304822daca1da2d2b88caf7e04f182ab9e49a" /d:sonar.cs.opencover.reportsPaths="#{NAME}.Test/coverage.opencover.xml" /v:"#{VERSION}"`
	puts `dotnet test #{NAME}.Test/#{NAME}.Test.csproj /p:CollectCoverage=true /p:CoverletOutputFormat=opencover`
	puts `dotnet sonarscanner end  /d:sonar.login="b74304822daca1da2d2b88caf7e04f182ab9e49a"`
end

task :publish => [:package] do
	FileUtils.cp("#{NAME}/bin/Release/#{NAME}.#{VERSION}.nupkg","#{NAME}.#{VERSION}.nupkg")
	local="#{Environment.dev_root}\\nuget"
	list=`nuget list #{NAME} -Source #{local}`
	if(!list.include?("#{NAME} #{VERSION}"))
		puts "nuget add #{NAME}.#{VERSION}.nupkg -Source #{local}"
		puts `nuget add #{NAME}.#{VERSION}.nupkg -Source #{local}`
	end
	list=`nuget list #{NAME} -Source https://api.nuget.org/v3/index.json`
	if(!list.include?("#{NAME} #{VERSION}"))
		puts "nuget push #{NAME}.#{VERSION}.nupkg -Source https://api.nuget.org/v3/index.json"
		puts `nuget push #{NAME}.#{VERSION}.nupkg -Source https://api.nuget.org/v3/index.json`
	end
end

task :default