# Trestle.Json.Serialization # 

[![NuGet Version and Downloads count](https://buildstats.info/nuget/Trestle)](https://www.nuget.org/packages/Trestle)

![Sonarcloud Status](https://sonarcloud.io/api/project_badges/measure?project=trestle.json.serialization&metric=alert_status)
![Sonarcloud LOC](https://sonarcloud.io/api/project_badges/measure?project=trestle.json.serialization&metric=ncloc)
![Sonarcloud Code Coverage](https://sonarcloud.io/api/project_badges/measure?project=trestle.json.serialization&metric=coverage)

Trestle.Json.Serialization provides functionality to serialize and deserialization JSON data.

## Downloads ##

The latest release of Trestle.Json.Serialization is [available on NuGet](https://www.nuget.org/packages/Trestle.Json.Serialization/).
It is also bundled as part of the [Trestle NuGet package](https://www.nuget.org/packages/Trestle/).

## Serialization ##
Serialization to JSON of types implementing the System.ISerializable interface is supported by the class Trestle.Json.Serialization.JsonFormatter.

Example Usage:
```C#
using System.IO;
using Trestle.Json.Serialization;

class Program
{
  void static Main(string[] args)
  {
    var formatter = new JsonFormatter();
    var data = new Dictionary<string,string>
    {
      {"name","test"},
      {"description","an example dataset"}
    };
    using(var memory = new MemoryStream())
    {
      formatter.Serialize(memory,data);
    }
  }
}
```

## Deserialization ##
