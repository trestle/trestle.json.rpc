﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Trestle.Extensions;

namespace Trestle.Json.Rpc
{
	/// <summary>
	/// JsonRPC Reponse
	/// </summary>
	[Serializable]
	public sealed class Response : ISerializable
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="id"></param>
		public Response(int id) : this(new Error(ErrorCode.InternalError), id)
		{
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="result"></param>
		/// <param name="id"></param>
		public Response(object result, int id)
		{
			Id = id; _result = result;
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="error"></param>
		/// <param name="id"></param>
		public Response(Error error, int id)
		{
			Id = id; _error = error;
		}

		private Response(SerializationInfo info, StreamingContext context)
		{
			Id = info.GetInt32("id");
			if (info.Contains("result"))
			{
				_result = info.GetValue("result", typeof(object));
			}

			if (info.Contains("error"))
			{
				var e = info.GetValue("error", typeof(ISerializable)) as ISerializable;
				_error = e.Convert<Error>();
			}
		}

		/// <summary>
		/// Get the SerializationInfo
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("jsonrpc", "2.0");
			info.AddValue("id", Id);
			if (Error != null)
			{
				info.AddValue("error", Error);
			}
			else
			{
				info.AddValue("result", Result);
			}
		}

		/// <summary>
		/// The response id should match the request id
		/// </summary>
		public int Id { get; }

		/// <summary>
		/// The result
		/// </summary>
		public object Result { get { return _result; } }

		[NonSerialized]
		private readonly object _result;

		/// <summary>
		/// The Error (may be null)
		/// </summary>
		public Error Error { get { return _error; } }

		[NonSerialized]
		private readonly Error _error;
	}
}