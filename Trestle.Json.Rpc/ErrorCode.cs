﻿namespace Trestle.Json.Rpc
{
	/// <summary>
	/// Error Codes
	/// </summary>
	public enum ErrorCode
	{
		/// <summary>
		/// Parse Error
		/// </summary>
		ParseError = -32700,

		/// <summary>
		/// Internal Error
		/// </summary>
		InternalError = -32603,

		/// <summary>
		/// Invalid Parameters
		/// </summary>
		InvalidParameters = -32602,

		/// <summary>
		/// Method Not Found
		/// </summary>
		MethodNotFound = -32601,

		/// <summary>
		/// Invalid Request
		/// </summary>
		InvalidRequest = -32600
	}
}