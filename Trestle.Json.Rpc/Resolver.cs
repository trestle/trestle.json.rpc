﻿using System.Text;

namespace Trestle.Json.Rpc
{
	/// <summary>
	/// Responds to a request by marshalling between a Request/Response and an IInvoke binding
	/// </summary>
	public class Resolver
	{
		/// <summary>
		/// Respond to a request
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		public Response Respond(Request request)
		{
			try
			{
				// what are the parameters required by the _invoke?
				var types = _invoke.ParameterTypes;

				var parameters = request.GetParameterArray();
				if (types.Count == parameters.Length)
				{
					var result = _invoke.Invoke(parameters);
					return new Response(result, request.Id);
				}
				else
				{
					return new Response(new Error(ErrorCode.InvalidParameters, "parameter count was incorrect"),
										request.Id);
				}
			}
			catch
			{
				return new Response(new Error(ErrorCode.InternalError, "internal error"), request.Id);
			}
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="invoke"></param>
		public Resolver(Trestle.Bindings.IInvoke invoke)
		{
			_invoke = invoke;
		}

		private readonly Trestle.Bindings.IInvoke _invoke;
	}
}