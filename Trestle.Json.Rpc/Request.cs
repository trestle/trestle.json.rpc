﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Trestle.Extensions;

namespace Trestle.Json.Rpc
{
	/// <summary>
	/// A request
	/// </summary>
	[Serializable]
	public sealed class Request : ISerializable
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="method"></param>
		public Request(string method)
		{
			Id = (int)DateTime.Now.Ticks;
			Method = method;
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="method"></param>
		/// <param name="parameters"></param>
		public Request(string method, object[] parameters) : this(method)
		{
			SetParameters(parameters);
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="method"></param>
		/// <param name="parameters"></param>
		public Request(string method, Dictionary<string, object> parameters) : this(method)
		{
			SetParameters(parameters);
		}

		/// <summary>
		/// Set the paramaters
		/// </summary>
		/// <param name="parameters"></param>
		public void SetParameters(IEnumerable parameters)
		{
			_parameters = new List<object>();
			foreach (var p in parameters) { _parameters.Add(p); }
		}

		/// <summary>
		/// Set the parameters
		/// </summary>
		/// <param name="parameters"></param>
		public void SetParameters(IDictionary parameters)
		{
			_parameters = new List<object>();
			_parameterNames = new List<string>();
			foreach (var key in parameters.Keys)
			{
				_parameters.Add(parameters[key]);
				_parameterNames.Add(key.ToString());
			}
		}

		private Request(SerializationInfo info, StreamingContext context)
		{
			Id = info.GetInt32("id");
			Method = info.GetString("method");
			if (info.Contains("params") && info.GetValue("params", typeof(IEnumerable)) is IEnumerable edata)
			{
				var pdata = edata as IDictionary;
				if (pdata is null)
				{
					SetParameters(edata);
				}
				else
				{
					SetParameters(pdata);
				}
			}
		}

		/// <summary>
		/// Get the SerializationInfo
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("jsonrpc", "2.0");
			info.AddValue("id", Id);
			info.AddValue("method", Method);

			if (_parameterNames != null && GetParametersNames().Count > 0)
			{
				info.AddValue("params", GetParameterDictionary());
			}
			else
			{
				var p = GetParameterArray();
				if (p.Length > 0)
				{
					info.AddValue("params", p);
				}
			}
		}

		/// <summary>
		/// Clone
		/// </summary>
		/// <returns></returns>
		public Request Clone()
		{
			return Trestle.Extensions.ISerializableExtension.Clone(this) as Request;
		}

		/// <summary>
		/// The Id
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// The method name
		/// </summary>
		public string Method { get; }

		/// <summary>
		/// Get the parameter array
		/// </summary>
		/// <returns></returns>
		public object[] GetParameterArray()
		{
			if (_parameters != null)
			{
				return _parameters.ToArray();
			}

			return Array.Empty<object>();
		}

		/// <summary>
		/// Get the parameters as a dictionary
		/// </summary>
		/// <returns></returns>
		public Dictionary<string, object> GetParameterDictionary()
		{
			var names = GetParametersNames();
			var parameters = GetParameterArray();
			var data = new Dictionary<string, object>();
			for (int i = 0; i < parameters.Length; ++i)
			{
				var name = i.ToString();
				if (names.Count > i)
				{
					name = names[i];
				}

				data.Add(name, parameters[i]);
			}
			return data;
		}

		/// <summary>
		/// Get the parameter names
		/// </summary>
		/// <returns></returns>
		public List<string> GetParametersNames()
		{
			return _parameterNames;
		}

		[NonSerialized]
		private List<object> _parameters;

		[NonSerialized]
		private List<string> _parameterNames;
	}
}