﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Trestle.Extensions;

namespace Trestle.Json.Rpc
{
	/// <summary>
	/// JsonRpc Error
	/// </summary>
	[Serializable]
	public sealed class Error : ISerializable
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="code">the error code</param>
		public Error(ErrorCode code)
		{
			Code = (int)code;
			switch ((ErrorCode)Code)
			{
				case ErrorCode.InternalError:
					Message = "internal error";
					break;

				case ErrorCode.InvalidParameters:
					Message = "invalid parameters";
					break;

				case ErrorCode.InvalidRequest:
					Message = "invalid request";
					break;

				case ErrorCode.MethodNotFound:
					Message = "method not found";
					break;

				case ErrorCode.ParseError:
					Message = "parse error";
					break;
			}
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="code"></param>
		/// <param name="message"></param>
		public Error(ErrorCode code, string message) : this(code)
		{
			Message = message;
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="code"></param>
		/// <param name="message"></param>
		/// <param name="data"></param>
		public Error(ErrorCode code, string message, object data) : this(code, message)
		{
			_data = data;
		}

		private Error(SerializationInfo info, StreamingContext context)
		{
			Code = info.GetInt32("code");
			Message = info.GetString("message");
			if (info.Contains("data"))
			{
				_data = info.GetValue("data", typeof(object));
			}
		}

		/// <summary>
		/// Get the SerializationInfo 
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("code", Code);
			info.AddValue("message", Message);
			if (Data != null)
			{
				info.AddValue("data", Data);
			}
		}

		/// <summary>
		/// Clone the Error
		/// </summary>
		/// <returns></returns>
		public Error Clone()
		{
			return Trestle.Extensions.ISerializableExtension.Clone(this) as Error;
		}

		/// <summary>
		/// A Code indicting the type of error
		/// </summary>
		public int Code { get; }

		/// <summary>
		/// A Message describing the error
		/// </summary>
		public string Message { get; }

		/// <summary>
		/// Addition data about the error
		/// </summary>
		public object Data { get { return _data; } }

		[NonSerialized]
		private readonly object _data;
	}
}