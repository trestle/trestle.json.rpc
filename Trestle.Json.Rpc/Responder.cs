﻿using System.Collections.Generic;
using System.Net;

namespace Trestle.Json.Rpc
{
	/// <summary>
	/// A class responsible for responding to Json-Rpc requests
	/// </summary>
	public sealed class Responder
	{
		/// <summary>
		/// Respond to a request
		/// </summary>
		/// <param name="request"></param>
		/// <returns>A Response</returns>
		public Response Respond(Request request)
		{
			if (Resolvers.ContainsKey(request.Method))
			{
				return Resolvers[request.Method].Respond(request);
			}
			return new Response(new Error(ErrorCode.MethodNotFound, $"method '{request.Method}' not found"), request.Id);
		}

		/// <summary>
		/// A Dictionary of method names to resolvers
		/// </summary>
		public Dictionary<string, Resolver> Resolvers { get; set; } = new Dictionary<string, Resolver>();
	}
}